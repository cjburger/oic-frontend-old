import { Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from './api.service';

export interface FormErrors {[key: string]: any; }


export abstract class FormComponent<T, T2 extends ApiService<T>> implements OnChanges {
  @Input() obj;
  @Output() objChange = new EventEmitter();
  form: FormGroup;
  fb = new FormBuilder;
  busy: boolean;
  submitted = false;
  abstract objHumanName: string; // The name to use in toasters.

  constructor(protected model, protected apiService: T2, protected toastrService: ToastrService) {
  }

  protected createForm() {
    this.form = this.fb.group({});
  };

  formErrors(): FormErrors {
    if (this.submitted && this.form.errors) {
      return this.form.errors;
    }
  }

  fieldHasErrors(name: string): boolean {
    let control = this.findFieldControl(name);
    if (control && (control.touched || this.submitted) && control.errors) {
      return true;
    }
  }

  fieldErrors(name: string): FormErrors {
    let control = this.findFieldControl(name);
    if (this.fieldHasErrors(name)) {
      return control.errors;
    } else {
      return {};
    }
  }

  protected findFieldControl(field: string): AbstractControl {
    let control: AbstractControl;
    // Convert Python variable name to Javascripts preferred camel case variable.
    let fieldCamelCase = field.replace(/_([a-z])/g, function (g) { return g[1].toUpperCase(); });

    if (field === 'non_field_errors') {
      // Non field errors
      control = this.form;
    } else if (this.form.contains(field)) {
      // Field error when field name matches API field name
      control = this.form.get(field);
    } else if (this.form.contains(fieldCamelCase)) {
      // Field error when field name matches converted API field name
      control = this.form.get(fieldCamelCase);
    } else {
      // Field is not defined in form but there is a validation error for it, set it globally
      control = this.form;
    }
    return control;
  }

  protected handleAddSuccess(obj) {
    this.obj = obj;
    this.objChange.emit(this.obj);
    this.busy = false;
  }

  protected handleUpdateSuccess(obj) {
    this.obj = obj;
    this.busy = false;
  }

  protected handleSubmitError(error: any) {
    if (error.status === 400) {
      const data = error.json();
      const fields = Object.keys(data || {});
      fields.forEach((field) => {
        const control = this.findFieldControl(field);
        const errors: FormErrors = {'server': data[field]};
        control.setErrors(errors);
      });
    }
    this.busy = false;
  }

  ngOnChanges() {
    this.form.patchValue(this.obj);
  }

  getData(): T {
    return new this.model(this.form.value);
  }

  onSubmit() {
    this.submitted = true;
    this.busy = true;
    if (this.obj.id) {
      // Update the instance
      this.apiService.update(this.obj.id, this.getData()).subscribe(
        entry => {
          this.handleUpdateSuccess(entry);
          this.toastrService.success(`${this.objHumanName} "${this.obj.name}" updated.`, 'Success');
        },
        error => {
          this.handleSubmitError(error);
        }
      );
    } else {
      // Create a new instance
      this.apiService.add(this.getData()).subscribe(
        entry => {
          this.handleAddSuccess(entry);
          this.toastrService.success(`${this.objHumanName} "${this.obj.name}" added.`, 'Success');
        },
        error => {
          this.handleSubmitError(error);
        }
      );
    }
  }
}
