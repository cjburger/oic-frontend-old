import { Expose, Transform, Exclude } from 'class-transformer';

export enum HardwareTypes {
  on_board,
  ethernet
}

export function transformType(value, type) {
  if (value === null || value === '') {
    return null;
  }
  return type(value);
}

export function transformEmptyStringToNullType(value) {
  if (value === null || value === '') {
    return null;
  }
  return value;
}

export class Pump {
  id?: number;
  name: string;
  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'hardware_type' })
  hardwareType: HardwareTypes;
  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'onboard_pump_id' })
  onboardPumpId: number;
  @Transform(value => transformType(value, Number))
  @Expose({ name: 'pressure_sensor' })
  pressureSensor: number;
  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'pressure_min' })
  pressureMin: number;
  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'pressure_max' })
  pressureMax: number;
  @Transform(value => transformType(value, Boolean))
  @Expose({ name: 'ignore_errors' })
  ignoreErrors: boolean;
  @Expose({ name: 'simultaneous_zones' })
  @Transform(value => transformEmptyStringToNullType(value))
  simultaneousZones: number;
  @Transform(value => transformType(value, Boolean))
  @Expose({ name: 'monitor_ac' })
  monitorAc: boolean;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class HardwareType {
  value: string;
  label: string;
}

export class OnBoardPump {
  id: number;
  desc: string;
}

export class PressureSensor {
  id?: number;
  name: string;

  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'hardware_type' })
  hardwareType: HardwareTypes;

  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'zero_offset' })
  zeroOffset: number;

  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'calibration_slope' })
  calibrationSlope: number;

  @Exclude()
  @Expose({ name: 'current_pressure' })
  currentPressure?: number;

  @Exclude()
  @Expose({ name: 'pressure_history' })
  pressureHistory?: number[];

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class SolenoidController {
  id?: number;
  name: string;

  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'hardware_type' })
  hardwareType: HardwareTypes;

  @Transform(value => transformEmptyStringToNullType(value))
  @Expose({ name: 'total_outputs' })
  totalOutputs: number;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
