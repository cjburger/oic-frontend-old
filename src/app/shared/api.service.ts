import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Pump, OnBoardPump, PressureSensor, SolenoidController, HardwareType } from './models';
import { plainToClass, classToPlain } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';

export abstract class ApiService<T> {
  private baseUrl = '/api/';
  protected url: string;
  model: ClassType<T>;

  constructor(protected http: Http) {}

  protected getUrl(id?: number | string) {
    let base = this.baseUrl + this.url;
    if (id) {
      return base + id + '/';
    }
    return base;
  }

  getAll(): Observable<T[]> {
    return this.http.get(this.getUrl()).map(res => res.json()).map(res => plainToClass(this.model, res as Object[]));
  }

  update(id: number, obj: T): Observable<T> {
    return this.http.patch(this.getUrl(id), classToPlain(obj)).map(res => res.json()).map(res => plainToClass(this.model, res as Object));
  }

  add(obj: T): Observable<T> {
    return this.http.post(this.getUrl(), classToPlain(obj)).map(res => res.json()).map(res => plainToClass(this.model, res as Object));
  }

  delete(id: number | string): Observable<any> {
    return this.http.delete(this.getUrl(id)).map(res => res.json());
  }
}

@Injectable()
export class HardwareTypesService extends ApiService<HardwareType> {
  protected url = 'hardware_types/';
  model = HardwareType;
  hardware_types: HardwareType[];

  constructor(http: Http) {
    super(http);
  }

  /**
   * Only retrieves the hardware types form the API once, then returns the cached types.
   */
  getAll(): Observable<HardwareType[]> {
    return Observable.create(observer => {
      if (this.hardware_types) {
        observer.next(this.hardware_types);
        observer.complete();
      } else {
        this.http.get(this.getUrl())
          .map(res => res.json())
          .map(res => plainToClass(this.model, res as Object[]))
          .subscribe(types => {
            this.hardware_types = types;
            observer.next(this.hardware_types);
            observer.complete();
          });
      }
    })
  }
}

@Injectable()
export class PumpService extends ApiService<Pump> {
  protected url = 'pumps/';
  model = Pump;

  constructor(http: Http) {
    super(http);
  }
}

@Injectable()
export class OnBoardPumpService extends ApiService<OnBoardPump> {
  protected url = 'onboard_pump_outputs/';
  model = OnBoardPump;

  constructor(http: Http) {
    super(http);
  }
}

@Injectable()
export class PressureSensorService extends ApiService<PressureSensor> {
  protected url = 'pressure_sensors/';
  model = PressureSensor;

  constructor(http: Http) {
    super(http);
  }
}

@Injectable()
export class SolenoidControllerService extends ApiService<SolenoidController> {
  protected url = 'solenoid_controllers/';
  model = SolenoidController;

  constructor(http: Http) {
    super(http);
  }
}
