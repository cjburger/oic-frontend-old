import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'my-form-errors',
  template: `
    <div *ngFor="let message of _errors.server" class="alert alert-danger" role="alert">
      {{ message }}
    </div>
  `
})
export class FormErrorsComponent implements OnChanges {
  @Input() errors: String[];
  _errors: {[key: string]: any; };

  constructor() { }

  ngOnChanges() {
    this._errors = this.errors || {};
  }

}
