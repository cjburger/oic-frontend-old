import { Component, Input, OnChanges } from '@angular/core';
import { trigger, style, transition, animate } from '@angular/core';

@Component({
  selector: 'my-field-errors',
  template: `
    <div *ngIf="_errors.required" class="form-control-feedback" [@errorAnim]>
      This field is required.
    </div>
    <div *ngFor="let message of _errors.server" class="form-control-feedback" [@errorAnim]>
      {{ message }}
    </div>
  `,
  animations: [
    trigger('errorAnim', [
      transition(':enter', [
        style({opacity: 0}),
        animate('0.3s ease-in')
      ]),
      transition(':leave', [
        animate('0.3s 10 ease-out', style({
          opacity: '0'
        }))
      ])
    ])
  ]
})
export class FieldErrorsComponent implements OnChanges {
  @Input() errors: {[key: string]: any; };
  _errors: {[key: string]: any; };

  constructor() {}

  ngOnChanges() {
    this._errors = this.errors || {};
  }

}
