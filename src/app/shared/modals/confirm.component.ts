// tslint:disable:no-access-missing-member
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';

export interface ConfirmModel {
  title: string;
  message: string;
  confirmButtonText: string;
  confirmButtonClass?: string;
}

@Component({
  selector: 'my-confirm',
  template: `
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{title || 'Confirm'}}</h5>
        <button type="button" class="close" (click)="close()" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <p>{{message || 'Are you sure?'}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" [ngClass]="getButtonClass()" (click)="confirm()">{{confirmButtonText || 'OK'}}</button>
        <button type="button" class="btn btn-secondary"  (click)="close()" >Cancel</button>
      </div>
    </div>
  </div>`
})
export class ConfirmComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  title: string;
  message: string;
  confirmButtonText: string;
  confirmButtonClass?: string;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = true;
    this.close();
  }

  getButtonClass() {
    return this.confirmButtonClass || 'btn-primary'
  }
}
