import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { UIRouterModule, UIView } from '@uirouter/angular';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ToastrModule } from 'ngx-toastr';
import { NgLoadingBarModule } from 'ng-loading-bar';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent, HeaderContentComponent, HeaderActionsService, SettingsHeaderActionsComponent } from './header/';
import { SideNavComponent }  from './sidenav/sidenav.component';
import { SideNavParentLiDirective } from './sidenav/parent-li.directive';
import { LoginComponent } from './login/login.component';
import { SettingsModule } from './settings/settings.module';
import {
  PumpService,
  OnBoardPumpService,
  PressureSensorService,
  SolenoidControllerService,
  HardwareTypesService
} from './shared';
import { AppStates } from './app.states';
import { routerConfigFn } from './router.config';

import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    UIRouterModule.forRoot({
      states: AppStates,
      otherwise: { state: 'app', params: {} },
      useHash: false,
      config: routerConfigFn,
    }),
    BootstrapModalModule.forRoot({container: document.body}),
    ToastrModule.forRoot(),
    NgLoadingBarModule.forRoot(),
    SettingsModule,
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    HeaderContentComponent,
    SettingsHeaderActionsComponent,
    SideNavComponent,
    SideNavParentLiDirective,
    LoginComponent,
  ],
  providers: [
    PressureSensorService,
    PumpService,
    OnBoardPumpService,
    SolenoidControllerService,
    HeaderActionsService,
    HardwareTypesService,
  ],
  bootstrap: [UIView]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
