import { Component, HostBinding } from '@angular/core';
import '../style/app.scss';

@Component({
  selector: 'my-app', // <my-app></my-app>
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @HostBinding('class.sidenav-active') sideNavOpen: boolean;
  noHeaderSidebar: boolean;

  constructor() {
    this.sideNavOpen = false;
  }
}
