import { UIRouterModule } from '@uirouter/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingStates } from './settings.states';
import { SettingsComponent } from './settings.component';
import { PressureSensorFormComponent, PressureSensorListComponent } from './pressure-sensors';
import { SolenoidControllerFormComponent, SolenoidControllerListComponent } from './solenoid-controllers';
import { PumpListComponent, PumpFormComponent } from './pump';
import { ZoneListComponent } from './zone/zone.component';
import { MyCapitalizePipe } from '../shared/pipes';
import { FieldErrorsComponent } from '../shared/form-errors/field-errors.component';
import { FormErrorsComponent } from '../shared/form-errors/form-errors.component';
import { ConfirmComponent } from '../shared/modals/confirm.component';

@NgModule({
  imports: [
    UIRouterModule.forChild({ states: SettingStates }),
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SettingsComponent,
    PressureSensorFormComponent,
    PressureSensorListComponent,
    PumpListComponent,
    PumpFormComponent,
    SolenoidControllerFormComponent,
    SolenoidControllerListComponent,
    ZoneListComponent,
    MyCapitalizePipe,
    FieldErrorsComponent,
    FormErrorsComponent,
    ConfirmComponent,
  ],
  entryComponents: [
    ConfirmComponent
  ],
})
export class SettingsModule { }
