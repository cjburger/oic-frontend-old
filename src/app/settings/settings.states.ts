import { Ng2StateDeclaration } from '@uirouter/angular';
import { PressureSensorListComponent } from './pressure-sensors'
import { PumpListComponent } from './pump';
import { SolenoidControllerListComponent } from './solenoid-controllers';
import { ZoneListComponent } from './zone/zone.component';
import { SettingsComponent } from './settings.component';
import { SettingsHeaderActionsComponent } from '../header/settings-actions.components'

export let SettingStates: Ng2StateDeclaration[] = [
  {
    parent: 'app',
    name: 'settings',
    url: '/settings',
    redirectTo: 'settings.pumps',
    data: {
      'title': 'Settings'
    },
    views: {
      $default: { component: SettingsComponent },
      actionsContent: { component: SettingsHeaderActionsComponent }
    },
  },
  {
    name: 'settings.pressure-sensors',
    url: '/pressure-sensors',
    component: PressureSensorListComponent,
    data: {
      'title': 'Pressure Sensors'
    }
  },
  {
    name: 'settings.pumps',
    url: '/pumps',
    component: PumpListComponent,
    data: {
      'title': 'Pumps'
    }
  },
  {
    name: 'settings.solenoid-controllers',
    url: '/solenoid-controllers',
    component: SolenoidControllerListComponent,
    data: {
      'title': 'Solenoid Controllers'
    }
  },
  {
    name: 'settings.zones',
    url: '/zones',
    component: ZoneListComponent,
    data: {
      'title': 'Zones'
    }
  },
];
