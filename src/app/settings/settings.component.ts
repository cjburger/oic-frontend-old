import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-settings',
  template: '<ui-view></ui-view>'
})
export class SettingsComponent implements OnInit {

  constructor() {
    // Do stuff
  }

  ngOnInit() {
    console.log('Hello from Settings');
  }

}
