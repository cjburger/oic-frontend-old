import { Component, OnInit } from '@angular/core';
import { StateService } from '@uirouter/core';

@Component({
  selector: 'my-zone-list',
  templateUrl: './zone.component.html'
})
export class ZoneListComponent implements OnInit {
  currentState;

  constructor(private stateService: StateService) {
    // Do stuff
    this.currentState = stateService.$current;
  }

  ngOnInit() {
    console.log('Hello from Zone List');
    console.log('State', this.currentState);
  }

  goDashboard() {
    this.stateService.go('dashboard');
  }

}
