import { Component, OnInit, OnDestroy } from '@angular/core';
import { PumpService, OnBoardPumpService, PressureSensorService } from '../../shared/api.service';
import { Pump, OnBoardPump, PressureSensor } from '../../shared/models';
import { HeaderActionsService, HeaderClickEvent } from '../../header/header.service';
import { ConfirmComponent } from '../../shared/modals/confirm.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { ToastrService } from 'ngx-toastr';
import { trigger, style, transition, animate } from '@angular/core';


@Component({
  selector: 'my-pump-list',
  templateUrl: './pump.component.html',
  animations: [
    trigger('itemAnim', [
      transition(':enter', [
        style({transform: 'translateY(-100%)', opacity: 0}),
        animate('0.3s ease-in')
      ]),
      transition(':leave', [
          animate('0.3s 10 ease-out', style({
            transform: 'translateX(+100%)',
            opacity: '0'
          }))
      ])
    ])
  ]
})
export class PumpListComponent implements OnInit, OnDestroy {
  pumps: Pump[] = [];
  onBoardPumps: OnBoardPump[] = [];
  pressureSensors: PressureSensor[] = [];
  private subscription;

  constructor(
    private pumpService: PumpService,
    private onBoardPumpService: OnBoardPumpService,
    private pressureSensorService: PressureSensorService,
    private headerActionService: HeaderActionsService,
    private dialogService: DialogService,
    private toastrService: ToastrService
  ) { }

  getPumps () {
    this.pumpService.getAll().subscribe(
      (pumps: Pump[]) => {
        this.pumps = pumps;
      },
    );
  }

  deletePump(pump: Pump) {
    if (pump.id) {
      this.dialogService.addDialog(ConfirmComponent, {
        title: 'Confirm',
        message: `Are you sure you want to delete the pump "${pump.name}"?`,
        confirmButtonText: 'Delete',
        confirmButtonClass: 'btn-danger',
      }, {closeByClickingOutside: true})
        .subscribe((isConfirmed) => {
          if (isConfirmed) {
            this.pumpService.delete(pump.id).subscribe(
              () => {
                this.getPumps();
                this.toastrService.success(`Pump "${pump.name}" was deleted.`, 'Success');
                },
              () => this.toastrService.error('Unable to delete pump.', 'Error')
            );
          }
        });
    } else {
      this.pumps.splice(this.pumps.indexOf(pump), 1);
    }
  }

  headerEventReceived (event: HeaderClickEvent) {
    if (event === HeaderClickEvent.add) {
      this.pumps.unshift(new Pump({simultaneousZones: 1}));
      document.body.scrollTop = 0;
    }
  }

  ngOnInit() {
    this.getPumps();
    this.onBoardPumpService.getAll().subscribe(
      (onBoardPumps: OnBoardPump[]) => {
        this.onBoardPumps = onBoardPumps;
      },
    );
    this.pressureSensorService.getAll().subscribe(
      (pressureSensors: PressureSensor[]) => {
        this.pressureSensors = pressureSensors;
      }
    );
    this.subscription = this.headerActionService.emitter$
      .subscribe(item => this.headerEventReceived(item));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
