// tslint:disable:no-access-missing-member
// See https://github.com/mgechev/codelyzer/issues/191
import { Component, Input, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { PumpService, HardwareTypesService } from '../../shared/api.service';
import { Pump, HardwareType, HardwareTypes, OnBoardPump, PressureSensor } from '../../shared/models';
import { FormComponent } from '../../shared/form.component';
import { trigger, style, transition, animate } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'my-pump-form',
  templateUrl: './pumpForm.component.html',
  animations: [
    trigger('fieldAnim', [
      transition(':enter', [
        style({opacity: 0}),
        animate('0.5s ease-in')
      ]),
      transition(':leave', [
        animate('0.5s 10 ease-out', style({
          opacity: '0'
        }))
      ])
    ])
  ]
})
export class PumpFormComponent extends FormComponent<Pump, PumpService> implements OnInit {
  @Input() onBoardPumps: OnBoardPump[];
  @Input() pressureSensors: PressureSensor[];
  hardwareTypesEnum = HardwareTypes;
  hardwareTypes: HardwareType[];
  objHumanName = 'Pump';

  constructor(
    protected apiService: PumpService,
    protected toastrService: ToastrService,
    protected hardwareTypesService: HardwareTypesService
  ) {
    super(Pump, apiService, toastrService);
    this.createForm();
  }

  protected createForm() {
    this.form = this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(10)]) ],
      hardwareType: '',
      onboardPumpId: '',
      pressureSensor: '',
      pressureMin: '',
      pressureMax: '',
      simultaneousZones: '',
    });
  }

  ngOnInit() {
    this.hardwareTypesService.getAll().subscribe(hardwareTypes => this.hardwareTypes = hardwareTypes);
  }
}
