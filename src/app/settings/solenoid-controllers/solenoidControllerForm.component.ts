// tslint:disable:no-access-missing-member
// See https://github.com/mgechev/codelyzer/issues/191
import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { SolenoidControllerService, HardwareTypesService } from '../../shared/api.service';
import { SolenoidController, HardwareType } from '../../shared/models';
import { FormComponent } from '../../shared/form.component';
import { ToastrService } from 'ngx-toastr';
import { trigger, style, transition, animate } from '@angular/core';

@Component({
  selector: 'my-solenoid-controller-form',
  templateUrl: './SolenoidControllerForm.component.html',
  animations: [
    trigger('fieldAnim', [
      transition(':enter', [
        style({opacity: 0}),
        animate('0.5s ease-in')
      ]),
      transition(':leave', [
        animate('0.5s 10 ease-out', style({
          opacity: '0'
        }))
      ])
    ])
  ]
})
export class SolenoidControllerFormComponent extends FormComponent<SolenoidController, SolenoidControllerService> implements OnInit {
  hardwareTypes: HardwareType[];
  objHumanName = 'Solenoid Controller';

  constructor(
    protected apiService: SolenoidControllerService,
    protected toastrService: ToastrService,
    protected hardwareTypesService: HardwareTypesService
  ) {
    super(SolenoidController, apiService, toastrService);
    this.createForm();
  }

  protected createForm() {
    this.form = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      hardwareType: '',
      totalOutputs: '',
    });
  }

  ngOnInit() {
    this.hardwareTypesService.getAll().subscribe(hardwareTypes => this.hardwareTypes = hardwareTypes);
  }
}
