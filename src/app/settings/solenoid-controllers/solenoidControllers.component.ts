import { Component, OnInit, OnDestroy } from '@angular/core';
import { SolenoidControllerService } from '../../shared/api.service';
import { SolenoidController } from '../../shared/models';
import { HeaderActionsService, HeaderClickEvent } from '../../header/header.service';
import { ConfirmComponent } from '../../shared/modals/confirm.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { ToastrService } from 'ngx-toastr';
import { trigger, style, transition, animate } from '@angular/core';

@Component({
  selector: 'my-solenoid-controller-list',
  templateUrl: './solenoidControllers.component.html',
  animations: [
    trigger('itemAnim', [
      transition(':enter', [
        style({transform: 'translateY(-100%)', opacity: 0}),
        animate('0.3s ease-in')
      ]),
      transition(':leave', [
          animate('0.3s 10 ease-out', style({
            transform: 'translateX(+100%)',
            opacity: '0'
          }))
      ])
    ])
  ]
})
export class SolenoidControllerListComponent implements OnInit, OnDestroy {
  solenoidControllers: SolenoidController[] = [];
  private subscription;

  constructor(
    private solenoidControllerService: SolenoidControllerService,
    private headerActionService: HeaderActionsService,
    private dialogService: DialogService,
    private toastrService: ToastrService
  ) { }

  getSolenoidControllers () {
    this.solenoidControllerService.getAll().subscribe(
      (solenoidControllers: SolenoidController[]) => {
        this.solenoidControllers = solenoidControllers;
      },
    );
  }

  deleteSolenoidController(solenoidController: SolenoidController) {
    if (solenoidController.id) {
      this.dialogService.addDialog(ConfirmComponent, {
        title: 'Confirm',
        message: `Are you sure you want to delete the solenoid controller "${solenoidController.name}"?`,
        confirmButtonText: 'Delete',
        confirmButtonClass: 'btn-danger',
      }, {closeByClickingOutside: true})
        .subscribe((isConfirmed) => {
          if (isConfirmed) {
            this.solenoidControllerService.delete(solenoidController.id).subscribe(
              () => {
                this.getSolenoidControllers();
                this.toastrService.success(`Solenoid controller "${solenoidController.name}" was deleted.`, 'Success');
                },
              () => this.toastrService.error('Unable to delete solenoid controller.', 'Error')
            );
          }
        });
    } else {
      this.solenoidControllers.splice(this.solenoidControllers.indexOf(solenoidController), 1);
    }
  }

  headerEventReceived (event: HeaderClickEvent) {
    if (event === HeaderClickEvent.add) {
      this.solenoidControllers.unshift(new SolenoidController());
      document.body.scrollTop = 0;
    }
  }

  ngOnInit() {
    this.getSolenoidControllers();
    this.subscription = this.headerActionService.emitter$
      .subscribe(item => this.headerEventReceived(item));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
