// tslint:disable:no-access-missing-member
// See https://github.com/mgechev/codelyzer/issues/191
import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { PressureSensorService, HardwareTypesService } from '../../shared/api.service';
import { PressureSensor, HardwareType } from '../../shared/models';
import { FormComponent } from '../../shared/form.component';
import { ToastrService } from 'ngx-toastr';
import { trigger, style, transition, animate } from '@angular/core';

@Component({
  selector: 'my-pressure-sensor-form',
  templateUrl: './pressureSensorForm.component.html',
  animations: [
    trigger('fieldAnim', [
      transition(':enter', [
        style({opacity: 0}),
        animate('0.5s ease-in')
      ]),
      transition(':leave', [
        animate('0.5s 10 ease-out', style({
          opacity: '0'
        }))
      ])
    ])
  ]
})
export class PressureSensorFormComponent extends FormComponent<PressureSensor, PressureSensorService> implements OnInit {
  hardwareTypes: HardwareType[];
  objHumanName = 'Pressure Sensor';

  constructor(
    protected apiService: PressureSensorService,
    protected toastrService: ToastrService,
    protected hardwareTypesService: HardwareTypesService
  ) {
    super(PressureSensor, apiService, toastrService);
    this.createForm();
  }

  protected createForm() {
    this.form = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      hardwareType: '',
      zeroOffset: '',
      calibrationSlope: ''
    });
  }

  ngOnInit() {
    this.hardwareTypesService.getAll().subscribe(hardwareTypes => this.hardwareTypes = hardwareTypes);
  }
}
