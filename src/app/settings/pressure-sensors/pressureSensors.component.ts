import { Component, OnInit, OnDestroy } from '@angular/core';
import { PressureSensorService } from '../../shared/api.service';
import { PressureSensor } from '../../shared/models';
import { HeaderActionsService, HeaderClickEvent } from '../../header/header.service';
import { ConfirmComponent } from '../../shared/modals/confirm.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { ToastrService } from 'ngx-toastr';
import { trigger, style, transition, animate } from '@angular/core';

@Component({
  selector: 'my-pressure-sensor-list',
  templateUrl: './pressureSensors.component.html',
  animations: [
    trigger('itemAnim', [
      transition(':enter', [
        style({transform: 'translateY(-100%)', opacity: 0}),
        animate('0.3s ease-in')
      ]),
      transition(':leave', [
          animate('0.3s 10 ease-out', style({
            transform: 'translateX(+100%)',
            opacity: '0'
          }))
      ])
    ])
  ]
})
export class PressureSensorListComponent implements OnInit, OnDestroy {
  pressureSensors: PressureSensor[] = [];
  private subscription;

  constructor(
    private pressureSensorService: PressureSensorService,
    private headerActionService: HeaderActionsService,
    private dialogService: DialogService,
    private toastrService: ToastrService
  ) { }

  getPressureSensors () {
    this.pressureSensorService.getAll().subscribe(
      (pressureSensors: PressureSensor[]) => {
        this.pressureSensors = pressureSensors;
      },
    );
  }

  deletePressureSensor(pressureSensor: PressureSensor) {
    if (pressureSensor.id) {
      this.dialogService.addDialog(ConfirmComponent, {
        title: 'Confirm',
        message: `Are you sure you want to delete the pressure sensor "${pressureSensor.name}"?`,
        confirmButtonText: 'Delete',
        confirmButtonClass: 'btn-danger',
      }, {closeByClickingOutside: true})
        .subscribe((isConfirmed) => {
          if (isConfirmed) {
            this.pressureSensorService.delete(pressureSensor.id).subscribe(
              () => {
                this.getPressureSensors();
                this.toastrService.success(`Pressure Sensor "${pressureSensor.name}" was deleted.`, 'Success');
                },
              () => this.toastrService.error('Unable to delete pressure sensor.', 'Error')
            );
          }
        });
    } else {
      this.pressureSensors.splice(this.pressureSensors.indexOf(pressureSensor), 1);
    }
  }

  headerEventReceived (event: HeaderClickEvent) {
    if (event === HeaderClickEvent.add) {
      this.pressureSensors.unshift(new PressureSensor());
      document.body.scrollTop = 0;
    }
  }

  ngOnInit() {
    this.getPressureSensors();
    this.subscription = this.headerActionService.emitter$
      .subscribe(item => this.headerEventReceived(item));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
