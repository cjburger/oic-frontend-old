describe('App', function () {

  beforeEach(function () {
    browser.get('/');
  });

  it('should have a title', function () {
    expect(browser.getTitle()).toEqual("Angular 2 App | ng2-webpack");
  });

  it('should have <header>', function () {
    expect(element(by.css('my-app header')).isPresent()).toEqual(true);
  });

  it('should have <app>', function () {
    expect(element(by.css('my-app app')).isPresent()).toEqual(true);
  });

  it('should have a app title', function () {
    expect(element(by.css('app h1')).getText()).toEqual('Hello from Angular 2!');
  });

  it('should have <footer>', function () {
    expect(element(by.css('my-app footer')).getText()).toEqual("Webpack Angular 2 Starter");
  });

});
