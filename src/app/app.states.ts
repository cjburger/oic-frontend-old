import { AppComponent } from './app.component';
// import { HeaderContentComponent } from './header/header-content.component';
import { HomeComponent } from './home/home.component';
import { Ng2StateDeclaration, /*loadNgModule*/ } from '@uirouter/angular';

/** The top level state(s) */
export let AppStates: Ng2StateDeclaration[] = [
  // The top-level app state.
  // This state fills the root <ui-view></ui-view> (defined in index.html) with the AppComponent
  {
    name: 'app',
    component: AppComponent,
    redirectTo: 'dashboard'
  },
  {
    parent: 'app',
    name: 'dashboard',
    url: '/dashboard',
    views: {
      $default: { component: HomeComponent }
    },
    data: {
      'title': 'Dashboard'
    }
  }

  // This is the Future State for lazy loading the BazModule
  // { name: 'app.baz', url: '/baz', lazyLoad: loadNgModule('src/baz/baz.module') }
];
