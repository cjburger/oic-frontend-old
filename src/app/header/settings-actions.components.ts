import { Component } from '@angular/core';
import { HeaderActionsService, HeaderClickEvent } from './header.service';

@Component({
  selector: 'my-settings-header-actions',
  template: `
    <ul class="nav pull-right">
      <li>
        <button (click)="onAddClick()" type="button" class="btn btn-secondary btn-sm">
          <i class="material-icons">add</i>&nbsp;Add
        </button>
      </li>
    </ul>
  `,
  styles: [
    'ul {line-height: 38px}'
  ]
})
export class SettingsHeaderActionsComponent {

  constructor (private headerService: HeaderActionsService) { }

  onAddClick() {
    this.headerService.emitter$.emit(HeaderClickEvent.add);
  }
}
