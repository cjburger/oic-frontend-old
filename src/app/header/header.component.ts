import { Component, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { StateService, TransitionService } from '@uirouter/core';

@Component({
  selector: 'my-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  @ViewChild('hamburger') hamburger: ElementRef;
  sideNavOpenValue = false;
  @Input() sideNavOpen: boolean;
  @Output() sideNavOpenChange = new EventEmitter();
  title: string;

  constructor(private stateService: StateService, transitionService: TransitionService) {
    this.setTitle();
    transitionService.onSuccess({}, () => {
      this.setTitle();
    });
  }

  setTitle() {
    this.title = this.stateService.$current.data.title || 'Title not set.';
  }

  hamburgerClicked() {
    // This should set the variable after onDocumentClick has evaluated the state of the navbar.
    setTimeout(() => {
      this.sideNavOpen = !this.sideNavOpen;
      this.sideNavOpenChange.emit(this.sideNavOpen);
    }, 100);
  };
}
