import { Injectable, EventEmitter } from '@angular/core';

export enum HeaderClickEvent {
  add
}

@Injectable()
export class HeaderActionsService {
  public emitter$ = new EventEmitter<HeaderClickEvent>();
}
