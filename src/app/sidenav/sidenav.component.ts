import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TransitionService } from '@uirouter/core';

@Component({
  selector: 'my-side-nav',
  templateUrl: './sidenav.component.html'
})
export class SideNavComponent {
  @Input() sideNavOpen = false;
  @Output() sideNavOpenChange = new EventEmitter();

  constructor(transitionService: TransitionService) {
    transitionService.onSuccess({}, () => {
      this.closeSidenav();
    });
  }

  closeSidenav() {
    this.sideNavOpen = false;
    this.sideNavOpenChange.emit(false);
  }
}
