import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[mySideNavParentLi]',
})
export class SideNavParentLiDirective {
  constructor(public el: ElementRef) { }

  @HostListener('click', ['$event.target']) onClick(target) {
    let anchorClicked = this.el.nativeElement.getElementsByTagName('a')[0].contains(target);
    if (anchorClicked) {
      this.el.nativeElement.classList.toggle('active');
    }
  }
}
